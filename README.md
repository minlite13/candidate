# README #

candiDate is a dating app that leverages your political interest to make a match. 

### Open Source projects used ###

* [Google Gson](https://code.google.com/p/google-gson/)
* [Picasso](https://github.com/square/picasso)
* [Butter Knife](https://github.com/JakeWharton/butterknife)
* [CircleImageView](https://github.com/hdodenhof/CircleImageView)
* [Android-RobotoTextView](https://github.com/johnkil/Android-RobotoTextView)
* [ParallaxScroll](https://github.com/nirhart/ParallaxScroll)

### Who do I talk to? ###

* Miro Markaravanes <miromarkarian[at]gmail[dot]com> - Developer