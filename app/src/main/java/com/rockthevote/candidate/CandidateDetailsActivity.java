package com.rockthevote.candidate;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.devspark.robototextview.widget.RobotoTextView;
import com.rockthevote.candidate.type.Candidate;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CandidateDetailsActivity extends Activity {
    @InjectView(R.id.candidate_picture)
    ImageView mCandidatePicture;
    @InjectView(R.id.candidate_name)
    RobotoTextView mCandidateName;
    @InjectView(R.id.candidate_party)
    ImageView mCandidateParty;
    @InjectView(R.id.candidate_office)
    RobotoTextView mCandidateOffice;

    @InjectView(R.id.candidate_score)
    RobotoTextView mCandidateScore;

    @InjectView(R.id.yays)
    RobotoTextView mYays;

    @InjectView(R.id.nays)
    RobotoTextView mNays;


    Candidate candidate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_details);
        ButterKnife.inject(this);

        candidate = getIntent().getParcelableExtra("candidate");

        if(candidate == null) {
            Log.d(getClass().getSimpleName(), "Candidate not specified.");
            finish();
            return;
        }

        Picasso.with(CandidateDetailsActivity.this).load(candidate.picture).into(mCandidatePicture);

        mCandidateName.setText(candidate.name);

        if(candidate.politicalParty == Candidate.PoliticalParty.REPUBLICAN) {
            mCandidateParty.setImageResource(R.drawable.ic_republican);
        } else {
            mCandidateParty.setImageResource(R.drawable.ic_democratic);
        }

        mCandidateScore.setText(candidate.yay_factor);

        mCandidateOffice.setText(candidate.runningOffice);


        String yays = "";

        for(String yay : candidate.yays) {
            yays += yay + "\n";
        }

        mYays.setText(yays);

        String nays = "";

        for(String nay : candidate.nays) {
            nays += nay + "\n";
        }

        mNays.setText(nays);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.candidate_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
