package com.rockthevote.candidate;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.rockthevote.candidate.contentprovider.CandidatesContentProvider;
import com.rockthevote.candidate.database.Candidates;

public class DataPopulateActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_populate);

        ContentValues cv = new ContentValues();
        cv.put(Candidates.COLUMN_NAME, "Jeff Gorell");
        cv.put(Candidates.COLUMN_PICTURE, "http://api.ning.com/files/wi*oWNYzff-yl-5pw0s45zEH3Ise0hdgIRvD6VSIeBNnjTTQ*4XYu8dNDXR-laBW-obxj1Xqy5q2Jf*rTv5s-vnjLKRUX*cK/Gorell.jpg");
        cv.put(Candidates.COLUMN_YAYS, "[\"Arts\", \"Civil Liberties\", \"Conservative\", \"Education\", \"Animal Rights\"]");
        cv.put(Candidates.COLUMN_NAYS, "[\"Access to abortion\", \"LGBT Issues\", \"Liberal\", \"Gun Control\", \"Government regulated environmental issues\", \"Obamacare\", \"Immigration Reform\"]");
        cv.put(Candidates.COLUMN_MUSIC_TASTE, "[\"Coldplay\", \"U2\", \"Foo Fighters\"]" );
        cv.put(Candidates.COLUMN_POLITICAL_PARTY, "Republican");
        cv.put(Candidates.COLUMN_RUNNING_OFFICE, "CA-26: Ventura County");
        cv.put(Candidates.COLUMN_ZIPCODE, "93060");
        cv.put(Candidates.COLUMN_YAY_FACTOR, "47");

        getContentResolver().insert(CandidatesContentProvider.CONTENT_URI, cv);

        cv = new ContentValues();
        cv.put(Candidates.COLUMN_NAME, "Julia Brownley");
        cv.put(Candidates.COLUMN_PICTURE, "http://www.dems.gov/wp-content/uploads/2013/08/BrownleyJ-CA26D.jpg");
        cv.put(Candidates.COLUMN_YAYS, "[\"Access to abortion\", \"Arts\", \"LGBT Issues\", \"Gun Control\", \"Civil Liberties\", \"Liberal\", \"Education\", \"Government regulated environmental issues\", \"Obamacare\", \"Animal Rights\"]");
        cv.put(Candidates.COLUMN_NAYS, "[\"Conservative\", \"Immigration Reform\"]");
        cv.put(Candidates.COLUMN_MUSIC_TASTE, "[\"Celine Dion\", \"Beyonce\", \"Cher\"]" );
        cv.put(Candidates.COLUMN_POLITICAL_PARTY, "Democrat");
        cv.put(Candidates.COLUMN_RUNNING_OFFICE, "CA-26: Ventura County");
        cv.put(Candidates.COLUMN_ZIPCODE, "93060");
        cv.put(Candidates.COLUMN_YAY_FACTOR, "38");

        getContentResolver().insert(CandidatesContentProvider.CONTENT_URI, cv);

        cv = new ContentValues();
        cv.put(Candidates.COLUMN_NAME, "Brian Nestade");
        cv.put(Candidates.COLUMN_PICTURE, "http://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Brian_Nestande.jpg/220px-Brian_Nestande.jpg");
        cv.put(Candidates.COLUMN_YAYS, "[\"Arts\", \"Civil Liberties\", \"LGBT Issues\", \"Conservative\", \"Education\"]");
        cv.put(Candidates.COLUMN_NAYS, "[\"Access to abortion\", \"Animal Rights\", \"Liberal\", \"Gun Control\", \"Government regulated environmental issues\", \"Obamacare\", \"Immigration Reform\"]");
        cv.put(Candidates.COLUMN_MUSIC_TASTE, "[\"Linkin Park\", \"Rihanna\", \"Carrie Underwood\"]" );
        cv.put(Candidates.COLUMN_POLITICAL_PARTY, "Republican");
        cv.put(Candidates.COLUMN_RUNNING_OFFICE, "CA-36: Riverside");
        cv.put(Candidates.COLUMN_ZIPCODE, "92501");
        cv.put(Candidates.COLUMN_YAY_FACTOR, "30");

        getContentResolver().insert(CandidatesContentProvider.CONTENT_URI, cv);

        cv = new ContentValues();
        cv.put(Candidates.COLUMN_NAME, "Judy Chu");
        cv.put(Candidates.COLUMN_PICTURE, "http://www.pasadenanow.com/main/wp-content/uploads/2014/07/Rep.-Judy-Chu.jpg");
        cv.put(Candidates.COLUMN_YAYS, "[\"Access to abortion\", \"Arts\", \"Civil Liberties\", \"Gun Control\", \"Liberal\", \"Education\", \"Immigration Reform\", \"Obamacare\", \"Animal Rights\"]");
        cv.put(Candidates.COLUMN_NAYS, "[\"Conservative\", \"LGBT Issues\", \"Government regulated environmental issues\"]");
        cv.put(Candidates.COLUMN_MUSIC_TASTE, "[\"Fleetwood Mac\", \"U2\", \"Shakira\"]" );
        cv.put(Candidates.COLUMN_POLITICAL_PARTY, "Democrat");
        cv.put(Candidates.COLUMN_RUNNING_OFFICE, "CA-27: Monterey Park");
        cv.put(Candidates.COLUMN_ZIPCODE, "93060");
        cv.put(Candidates.COLUMN_YAY_FACTOR, "63");

        getContentResolver().insert(CandidatesContentProvider.CONTENT_URI, cv);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.data_populate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
