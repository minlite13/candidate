package com.rockthevote.candidate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.rockthevote.candidate.service.CandidateFilenameResponse;
import com.rockthevote.candidate.service.CandidateQueryResponse;
import com.rockthevote.candidate.service.CandidateService;
import com.rockthevote.candidate.util.ImageUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class EditProfileActivity extends Activity {

    @InjectView(R.id.picture_grid_1_1)
    View mPictureGrid11;
    @InjectView(R.id.picture_1)
    ImageView mPicture1;
    @InjectView(R.id.picture_grid_1_2)
    View mPictureGrid12;
    @InjectView(R.id.picture_2)
    ImageView mPicture2;
    @InjectView(R.id.picture_grid_2_1)
    View mPictureGrid21;
    @InjectView(R.id.picture_3)
    ImageView mPicture3;
    @InjectView(R.id.picture_grid_2_2)
    View mPictureGrid22;
    @InjectView(R.id.picture_4)
    ImageView mPicture4;

    @InjectView(R.id.bio)
    EditText mBio;

    @InjectView(R.id.save_profile)
    View mSaveProfile;

    private static final int REQ_CODE_PICK_IMAGE = 23;
    private static final int REQ_CODE_TAKE_IMAGE = 42;

    ImageView mSelectedPicture;
    int mSelectedItem;

    Uri tempPictureUri;

    boolean changed[];

    int mShouldUpload;
    int mUploaded;

    HashMap<Integer, Bitmap> bitmaps = new HashMap<>();

    String userId;
    String accessToken;

    ProgressDialog mProgressDialog;

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.inject(this);

        sharedPref = getSharedPreferences("com.rockthevote.candidate", MODE_PRIVATE);

        userId = sharedPref.getString("user_id", null);
        accessToken = sharedPref.getString("access_token", null);

        if(TextUtils.isEmpty(userId) || TextUtils.isEmpty(accessToken)) {
            // Empty shared preferences
            sharedPref.edit().clear().apply();

            startActivity(new Intent(EditProfileActivity.this, LandingActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }

        changed = new boolean[4];

        mPictureGrid11.setOnClickListener(new PictureOnClickListener(mPicture1, 0));
        mPictureGrid12.setOnClickListener(new PictureOnClickListener(mPicture2, 1));
        mPictureGrid21.setOnClickListener(new PictureOnClickListener(mPicture3, 2));
        mPictureGrid22.setOnClickListener(new PictureOnClickListener(mPicture4, 3));

        mSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShouldUpload = 0;
                mUploaded = 0;

                // First calculate how many pictures we need to upload
                for(boolean change : changed) {
                    if(change) mShouldUpload++;
                }

                if(mShouldUpload > 0) {

                    mProgressDialog = new ProgressDialog(EditProfileActivity.this);
                    mProgressDialog.setMessage(getString(R.string.saving));
                    mProgressDialog.setIndeterminate(true);
                    mProgressDialog.show();

                    for (int i = 0; i < changed.length; i++) {
                        if (changed[i]) {
                            Response.Listener<CandidateFilenameResponse> listener = new Response.Listener<CandidateFilenameResponse>() {
                                @Override
                                public void onResponse(CandidateFilenameResponse response) {
                                    mUploaded++;

                                    if (mUploaded == mShouldUpload) {
                                        // Uploading all pictures completed.
                                        Log.d(getClass().getSimpleName(), "All pictures uploaded!");

                                        // Save bio and proceed
                                        Response.Listener<CandidateQueryResponse> listener = new Response.Listener<CandidateQueryResponse>() {
                                            @Override
                                            public void onResponse(CandidateQueryResponse response) {
                                                // Saved profile successfully.

                                                mProgressDialog.dismiss();
                                                Toast.makeText(EditProfileActivity.this, "Saved!", Toast.LENGTH_SHORT).show();

                                                // Save that the user has edited the profile
                                                sharedPref.edit().putBoolean("edited_profile", true).apply();

                                                // TODO switch to questions screen
                                                startActivity(new Intent(EditProfileActivity.this, ProfileActivity.class));
                                                finish();
                                            }
                                        };

                                        Response.ErrorListener errorListener = new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // Error updating profile.
                                                mProgressDialog.dismiss();

                                                try {
                                                    Toast.makeText(EditProfileActivity.this, "Error saving profile: [" + error.networkResponse.statusCode + "] " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                                } catch (NullPointerException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        };

                                        CandidateService.getInstance().updateUserById(EditProfileActivity.this, listener, errorListener, userId, null, null, null, mBio.getText().toString(), null, null, accessToken);
                                    }
                                }
                            };

                            Response.ErrorListener errorListener = new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d(getClass().getSimpleName(), "Could not upload some of the pictures " + error.getMessage());

                                    mProgressDialog.dismiss();

                                    Toast.makeText(EditProfileActivity.this, "Could not upload some of the pictures " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            };

                            Bitmap picture = bitmaps.get(i);

                            Log.d(getClass().getSimpleName(), "Starting decode");
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            picture.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            String base64EncodedPicture = Base64.encodeToString(byteArray, Base64.DEFAULT);
                            Log.d(getClass().getSimpleName(), "Finished decode");

                            CandidateService.getInstance().postPicture(EditProfileActivity.this, listener, errorListener, userId, String.valueOf(i + 1), base64EncodedPicture, "png", accessToken);
                        }
                    }
                }
            }
        });
    }

    class PictureOnClickListener implements View.OnClickListener {

        ImageView target;
        int index;

        public PictureOnClickListener(ImageView target, int index) {
            this.target = target;
            this.index = index;
        }

        @Override
        public void onClick(View view) {
            mSelectedPicture = target;
            mSelectedItem = index;

            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setTitle(R.string.title_picture_prompt)
                    .setItems(R.array.picture_sources, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    // Pick a picture
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent,
                                            "Select a Picture"), REQ_CODE_PICK_IMAGE);
                                    break;
                                case 1:
                                    // Take a picture
                                    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                                        dispatchTakePictureIntent();
                                    } else {
                                        Toast.makeText(EditProfileActivity.this, "No camera has been found on this device.", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e("dispatchTakePictureIntent", "Error occurred while creating a new image file.");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQ_CODE_TAKE_IMAGE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        tempPictureUri = Uri.fromFile(image);
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQ_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Uri pictureUri = data.getData();

                        Bitmap picture = ImageUtils.bitmapFromUri(EditProfileActivity.this,
                                pictureUri,
                                mSelectedPicture.getWidth(),
                                mSelectedPicture.getHeight());
                        bitmaps.put(mSelectedItem, ImageUtils.scaledBitmapFromUriWithLongEdge(this, pictureUri, 480));
                        mSelectedPicture.setImageBitmap(picture);
                        changed[mSelectedItem] = true;
                    }
                }
                break;
            case REQ_CODE_TAKE_IMAGE:
                if (resultCode == RESULT_OK) {
                    Bitmap picture = ImageUtils.bitmapFromUri(EditProfileActivity.this,
                            tempPictureUri,
                            mSelectedPicture.getWidth(),
                            mSelectedPicture.getHeight());
                    bitmaps.put(mSelectedItem, ImageUtils.scaledBitmapFromUriWithLongEdge(this, tempPictureUri, 480));
                    mSelectedPicture.setImageBitmap(picture);
                    changed[mSelectedItem] = true;
                }
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
