package com.rockthevote.candidate;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
import com.rockthevote.candidate.service.CandidateAccessTokenResponse;
import com.rockthevote.candidate.service.CandidateService;
import com.rockthevote.candidate.service.CandidateUsersResponse;
import com.rockthevote.candidate.type.User;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class LoginActivity extends Activity {

    @InjectView(R.id.login_facebook)
    View mLoginFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        mLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Session.openActiveSession(LoginActivity.this, true, Arrays.asList("public_profile", "user_birthday", "email"), new Session.StatusCallback() {

                    // callback when session changes state
                    @Override
                    public void call(final Session session, SessionState state, Exception exception) {
                            if(session.isOpened()) {
                                // Session opened
                                // make request to the /me API
                                Request.newMeRequest(session, new Request.GraphUserCallback() {

                                    // callback after Graph API response with user object
                                    @Override
                                    public void onCompleted(final GraphUser user, Response response) {
//                                        new AlertDialog.Builder(LoginActivity.this)
//                                                .setTitle("Welcome")
//                                                .setMessage("Welcome, "+ user.getFirstName() + "!")
//                                                .setPositiveButton("OK", null)
//                                                .show();

                                        com.android.volley.Response.Listener<CandidateAccessTokenResponse> listener = new com.android.volley.Response.Listener<CandidateAccessTokenResponse>() {
                                            @Override
                                            public void onResponse(CandidateAccessTokenResponse response) {
                                                // DO something
                                                Log.d(getClass().getSimpleName(), "SUCCESS");

                                                if(response.isSuccess()) {
                                                    // Save the details
                                                    SharedPreferences sharedPref = getSharedPreferences("com.rockthevote.candidate", MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedPref.edit();

                                                    editor.putString("access_token", response.access_token);
                                                    editor.putString("access_token_expires_in", response.expires_in);
                                                    editor.putString("access_token_type", response.token_type);
                                                    editor.putString("access_token_scope", response.scope);

                                                    editor.apply();

                                                    // Get more details about the user
                                                    com.android.volley.Response.Listener<CandidateUsersResponse> listener = new com.android.volley.Response.Listener<CandidateUsersResponse>() {
                                                        @Override
                                                        public void onResponse(CandidateUsersResponse response) {

                                                            if(response.isSuccess() && response.rows.size() > 0) {
                                                                // Successfully retrieved user details

                                                                User user = response.rows.get(0);

                                                                SharedPreferences sharedPref = getSharedPreferences("com.rockthevote.candidate", MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = sharedPref.edit();

                                                                editor.putString("user_id", user.id);
                                                                editor.putString("user_facebook_id", user.facebook_id);
                                                                editor.putString("user", new Gson().toJson(user));

                                                                editor.apply();

                                                                startActivity(new Intent(LoginActivity.this, EditProfileActivity.class));
                                                                finish();
                                                            }

                                                        }
                                                    };

                                                    com.android.volley.Response.ErrorListener errorListener = new com.android.volley.Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            // Something went wrong
                                                            Log.d(getClass().getSimpleName(), "WENT WRONG");
                                                        }
                                                    };

                                                    CandidateService.getInstance().getUserByFacebookId(LoginActivity.this, listener, errorListener, user.getId(), response.access_token);
                                                }
                                            }
                                        };

                                        com.android.volley.Response.ErrorListener errorListener = new com.android.volley.Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // Something went wrong
                                                Log.d(getClass().getSimpleName(), "WENT WRONG");
                                            }
                                        };

                                        String birthday = user.getBirthday();

                                        String formattedBirthday = "";

                                        if(!TextUtils.isEmpty(birthday) && birthday.split("/").length == 3) {
                                            String[] birthdayChunks = birthday.split("/");

                                            formattedBirthday = birthdayChunks[2] + "-" + birthdayChunks[0] + "-" + birthdayChunks[1];
                                        }

                                        CandidateService.getInstance().signUpUser(LoginActivity.this, listener, errorListener, user.getFirstName(), user.getLastName(), formattedBirthday, (String) user.getProperty("email"), user.getProperty("gender").equals("male") ? "m" : "f", user.getId(), "http://graph.facebook.com/"+ user.getId() +"/picture?type=large", session.getAccessToken());
                                    }
                                }).executeAsync();
                            }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
}
