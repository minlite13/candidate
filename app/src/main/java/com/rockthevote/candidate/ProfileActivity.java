package com.rockthevote.candidate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.devspark.robototextview.widget.RobotoTextView;
import com.nirhart.parallaxscroll.views.ParallaxListView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ProfileActivity extends Activity {
    @InjectView(R.id.issues_view)
    ParallaxListView mIssuesView;

    HashMap<Integer, Boolean> yaysNays;

    IssuesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.inject(this);

        List<String> issues = Arrays.asList("Access to abortion", "Arts", "LGBT Issues", "Civil Liberties", "Conservative", "Liberal",
                "Independent", "Government regulated environmental issues", "Gun Control", "Obamacare", "Animal Rights", "Immigration Reform");

        yaysNays = new HashMap<>();

        mAdapter = new IssuesAdapter(this, R.layout.issue_item, issues);

        View headerview = getLayoutInflater().inflate(R.layout.profile_header, null, false);

        mIssuesView.addParallaxedHeaderView(headerview);

        mIssuesView.setAdapter(mAdapter);

        View footer = getLayoutInflater().inflate(R.layout.footer_next_screen, null, false);


        footer.findViewById(R.id.save_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, SwiperActivity.class);
                intent.putExtra("yaysnays", yaysNays);
                startActivity(intent);
                finish();
            }
        });

        mIssuesView.addFooterView(footer);


    }


    class IssuesAdapter extends ArrayAdapter<String> {

        int layoutId;

        public IssuesAdapter(Context context, int layoutId, List<String> items) {
            super(context, layoutId, items);
            this.layoutId = layoutId;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(layoutId, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mIssue.setText(getItem(position));

            holder.mYayView.setOnClickListener(new OnYayClickListener(position));
            holder.mNayView.setOnClickListener(new OnNayClickListener(position));

            Boolean yayNay = yaysNays.get(position);

            if (yayNay == null) {
                // Nothing selected
                holder.mYayImage.setImageResource(R.drawable.ic_thumb_up_unselected);
                holder.mNayImage.setImageResource(R.drawable.ic_thumb_down_unselected);
            } else if (yayNay) {
                holder.mYayImage.setImageResource(R.drawable.ic_thumb_up);
                holder.mNayImage.setImageResource(R.drawable.ic_thumb_down_unselected);
            } else {
                holder.mYayImage.setImageResource(R.drawable.ic_thumb_up_unselected);
                holder.mNayImage.setImageResource(R.drawable.ic_thumb_down);
            }

            return convertView;
        }

        class OnYayClickListener implements View.OnClickListener {
            Integer position;

            public OnYayClickListener(Integer position) {
                this.position = position;
            }

            @Override
            public void onClick(View view) {
                // Change the response to yay
                yaysNays.put(position, true);
                // Force redraw
                mAdapter.notifyDataSetChanged();
            }
        }

        class OnNayClickListener implements View.OnClickListener {
            Integer position;

            public OnNayClickListener(Integer position) {
                this.position = position;
            }

            @Override
            public void onClick(View view) {
                // Change the response to yay
                yaysNays.put(position, false);
                // Force redraw
                mAdapter.notifyDataSetChanged();
            }
        }


        class ViewHolder {
            @InjectView(R.id.issue)
            RobotoTextView mIssue;
            @InjectView(R.id.yay_view)
            View mYayView;
            @InjectView(R.id.yay_image)
            ImageView mYayImage;
            @InjectView(R.id.nay_view)
            View mNayView;
            @InjectView(R.id.nay_image)
            ImageView mNayImage;

            public ViewHolder(View view) {
                ButterKnife.inject(this, view);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.issues, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
