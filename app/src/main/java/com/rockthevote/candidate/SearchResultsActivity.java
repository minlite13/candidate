package com.rockthevote.candidate;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.devspark.robototextview.widget.RobotoButton;
import com.devspark.robototextview.widget.RobotoTextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rockthevote.candidate.contentprovider.CandidatesContentProvider;
import com.rockthevote.candidate.database.Candidates;
import com.rockthevote.candidate.type.Candidate;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SearchResultsActivity extends Activity {
    @InjectView(R.id.candidate_info)
    View mCandidateInfo;


    int windowwidth;
    int screenCenter;
    int Likes = 0;
    RelativeLayout parentView;

    float mLastTouchX;
    float mLastTouchY;

    float initialX;
    float initialY;
    float initialRotation;

    List<String> issues = Arrays.asList("Access to abortion", "Arts", "LGBT Issues", "Civil Liberties", "Conservative", "Liberal",
            "Independent", "Government regulated environmental issues", "Gun Control", "Obamacare", "Animal Rights", "Immigration Reform");

    List<Candidate> candidates;

    int passedItems = 0;

    View registerView;
    RobotoButton mRegisterToVote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        ButterKnife.inject(this);

        parentView = (RelativeLayout) findViewById(R.id.candidate_swiper);
        windowwidth = getWindowManager().getDefaultDisplay().getWidth();
        screenCenter = windowwidth / 2;

        candidates = new ArrayList<>();

        HashMap<Integer, Boolean> yaysNays = (HashMap<Integer, Boolean>) getIntent().getSerializableExtra("yaysnays");

        ContentResolver cr = getContentResolver();

        Cursor candidatesCursor = cr.query(CandidatesContentProvider.CONTENT_URI, null, null, null, null);

        Gson gson = new Gson();

        if (candidatesCursor.getCount() > 0) {
            while (candidatesCursor.moveToNext()) {
                Candidate candidate = new Candidate(candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_NAME)),
                        candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_PICTURE)),
                        (List<String>) gson.fromJson(candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_YAYS)), new TypeToken<List<String>>() {
                        }.getType()),
                        (List<String>) gson.fromJson(candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_NAYS)), new TypeToken<List<String>>() {
                        }.getType()),
                        (List<String>) gson.fromJson(candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_MUSIC_TASTE)), new TypeToken<List<String>>() {
                        }.getType()),
                        "Republican".equals(candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_POLITICAL_PARTY))) ? Candidate.PoliticalParty.REPUBLICAN : Candidate.PoliticalParty.DEMOCRAT,
                        candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_RUNNING_OFFICE)),
                        candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_ZIPCODE)),
                        candidatesCursor.getString(candidatesCursor.getColumnIndexOrThrow(Candidates.COLUMN_YAY_FACTOR))
                );

                candidates.add(candidate);
            }
        }

        Random random = new Random();

        this.candidates = filterCandidates(candidates, yaysNays);

        for (final Candidate candidate : candidates) {

            final View m_view = getLayoutInflater().inflate(R.layout.candidate_view, null, false);
            ImageView m_image = (ImageView) m_view.findViewById(R.id.sp_image);
            final RelativeLayout m_topLayout = (RelativeLayout) m_view
                    .findViewById(R.id.candidate_top_view);

            m_view.setY(100);
            Picasso.with(SearchResultsActivity.this).load(candidate.picture).into(m_image);

            ((RobotoTextView) m_view.findViewById(R.id.candidate_name)).setText(candidate.name);

            if (candidate.politicalParty == Candidate.PoliticalParty.REPUBLICAN) {
                ((ImageView) m_view.findViewById(R.id.candidate_party)).setImageResource(R.drawable.ic_republican);
            } else {
                ((ImageView) m_view.findViewById(R.id.candidate_party)).setImageResource(R.drawable.ic_democratic);
            }

            ((RobotoTextView) m_view.findViewById(R.id.candidate_office)).setText(candidate.runningOffice);

            ((RobotoTextView) m_view.findViewById(R.id.candidate_interests)).setVisibility(View.INVISIBLE);

            ((RobotoTextView) m_view.findViewById(R.id.candidate_score)).setText(candidate.yay_factor);

            m_view.setRotation(random.nextInt(10) - 5);

            m_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // startActivity(new Intent(SwiperActivity.this, DataPopulateActivity.class));
                    Intent intent = new Intent(SearchResultsActivity.this, CandidateDetailsActivity.class);
                    intent.putExtra("candidate", candidate);
                    startActivity(intent);
                }
            });

            // ADD dynamically like button on image.
            final View imageLike = m_view.findViewById(R.id.yay);
            // ADD dynamically dislike button on image.
            final View imagePass = m_view.findViewById(R.id.nay);

            // Touch listener on the image layout to swipe image right or left.
            m_topLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            mLastTouchX = event.getRawX();
                            mLastTouchY = event.getRawY();

                            initialX = m_view.getX();
                            initialY = m_view.getY();
                            initialRotation = m_view.getRotation();

                            break;
                        case MotionEvent.ACTION_MOVE:

                            float curX = event.getRawX();
                            float curY = event.getRawY();

                            float dX = curX - mLastTouchX;
                            float dY = curY - mLastTouchY;

                            m_view.setX(m_view.getX() + dX);
                            m_view.setY(m_view.getY() + dY);

                            m_view.setRotation((-dX / (float) (windowwidth / 4)) * 45.0f);

                            mLastTouchX = event.getRawX();
                            mLastTouchY = event.getRawY();

                            if (event.getRawX() >= screenCenter) {
                                if (event.getRawX() > (3 / (float) 4 * windowwidth)) {
                                    imageLike.setVisibility(View.VISIBLE);
                                    if (event.getRawX() > (windowwidth - (screenCenter / 4)) * 0.90) {
                                        Likes = 2;
                                    } else {
                                        Likes = 0;
                                    }
                                } else {
                                    Likes = 0;
                                    imageLike.setVisibility(View.INVISIBLE);
                                }
                                imagePass.setVisibility(View.INVISIBLE);
                            } else {
                                if (event.getRawX() < (screenCenter / 2)) {
                                    imagePass.setVisibility(View.VISIBLE);
                                    if (0.50 * event.getRawX() < (screenCenter / 4)) {
                                        Likes = 1;
                                    } else {
                                        Likes = 0;
                                    }
                                } else {
                                    Likes = 0;
                                    imagePass.setVisibility(View.INVISIBLE);
                                }
                                imageLike.setVisibility(View.INVISIBLE);
                            }

                            break;
                        case MotionEvent.ACTION_UP:
                            imagePass.setVisibility(View.INVISIBLE);
                            imageLike.setVisibility(View.INVISIBLE);

                            if (Likes == 0) {
                                m_view.animate().x(initialX).y(initialY).rotation(new Random().nextInt(10) - 5).setDuration(300).start();
                            } else if (Likes == 1) {
                                passedItems++;
                                m_view.animate().x(-(m_view.getWidth() + 50)).y(-(m_view.getHeight() + 50)).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
                                if (passedItems == candidates.size()) {
                                    inflateAndShowRegister();
                                    mRegisterToVote.setEnabled(true);
                                }
                            } else if (Likes == 2) {
                                passedItems++;
                                m_view.animate().x(windowwidth + 100).y(-m_view.getHeight()).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
                                if (passedItems == candidates.size()) {
                                    inflateAndShowRegister();
                                    mRegisterToVote.setClickable(true);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    return true;
                }
            });

            parentView.addView(m_view);
        }

        mCandidateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(SearchResultsActivity.this, CandidateDetailsActivity.class);
                    intent.putExtra("candidate", candidates.get(candidates.size() - passedItems - 1));
                    startActivity(intent);
                } catch (ArrayIndexOutOfBoundsException e) {
                    // Ignore
                }
            }
        });

        mCandidateInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                startActivity(new Intent(SearchResultsActivity.this, DataPopulateActivity.class));
                return false;
            }
        });
    }

    void inflateAndShowRegister() {
        registerView = getLayoutInflater().inflate(R.layout.register_to_vote_view, parentView, false);
        mRegisterToVote = (RobotoButton) registerView.findViewById(R.id.register_to_vote);

        mRegisterToVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rockthevote.com/"));
                startActivity(browserIntent);

                startActivity(new Intent(SearchResultsActivity.this, UserActivity.class));
            }
        });
        registerView.setAlpha(0);

        ((RelativeLayout) findViewById(R.id.wrapper)).addView(registerView);

        registerView.animate().alpha(1).setDuration(300).start();

        mCandidateInfo.setVisibility(View.GONE);
    }

    List<Candidate> filterCandidates(List<Candidate> candidates, HashMap<Integer, Boolean> yaynays) {

        List<Candidate> filteredCandidates = new ArrayList<>(candidates);

        for (Map.Entry<Integer, Boolean> entry : yaynays.entrySet()) {
            String issue = issues.get(entry.getKey());
            Boolean yay = entry.getValue();
            for (Candidate candidate : candidates) {
                if (yay) {
                    if(!candidate.yays.contains(issue))
                        filteredCandidates.remove(candidate);
                } else {
                    if(!candidate.nays.contains(issue))
                        filteredCandidates.remove(candidate);
                }
            }
        }

        return filteredCandidates;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
