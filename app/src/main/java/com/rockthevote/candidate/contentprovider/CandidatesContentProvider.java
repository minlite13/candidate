package com.rockthevote.candidate.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.rockthevote.candidate.database.Candidates;
import com.rockthevote.candidate.database.CandidatesDatabaseHelper;

import java.util.Arrays;
import java.util.HashSet;


public class CandidatesContentProvider extends ContentProvider {

    // database
    private CandidatesDatabaseHelper database;

    // used for the UriMatcher
    private static final int CONTACTS = 10;
    private static final int CONTACT_ID = 20;

    private static final String AUTHORITY = "com.rockthevote.candidate.candidatesprovider";

    private static final String BASE_PATH = "contacts";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, CONTACTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", CONTACT_ID);
    }

    @Override
    public boolean onCreate() {
        database = new CandidatesDatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(Candidates.TABLE_CANDIDATES);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case CONTACTS:
                break;
            case CONTACT_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(Candidates.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int id;
        switch (uriType) {
            case CONTACTS:
                try {
                    id = (int) sqlDB.insertWithOnConflict(Candidates.TABLE_CANDIDATES, null, values, SQLiteDatabase.CONFLICT_ABORT);
                } catch (SQLiteConstraintException e) {
                    id = sqlDB.update(Candidates.TABLE_CANDIDATES,
                            values,
                            Candidates.COLUMN_ID + "=?",
                            new String[] {values.getAsString(Candidates.COLUMN_ID)});
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted;
        switch (uriType) {
            case CONTACTS:
                rowsDeleted = sqlDB.delete(Candidates.TABLE_CANDIDATES, selection,
                        selectionArgs);
                break;
            case CONTACT_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(Candidates.TABLE_CANDIDATES,
                            Candidates.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(Candidates.TABLE_CANDIDATES,
                            Candidates.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated;
        switch (uriType) {
            case CONTACTS:
                rowsUpdated = sqlDB.update(Candidates.TABLE_CANDIDATES,
                        values,
                        selection,
                        selectionArgs);
                break;
            case CONTACT_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(Candidates.TABLE_CANDIDATES,
                            values,
                            Candidates.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(Candidates.TABLE_CANDIDATES,
                            values,
                            Candidates.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = { Candidates.COLUMN_NAME,
                Candidates.COLUMN_PICTURE,
                Candidates.COLUMN_YAYS,
                Candidates.COLUMN_NAYS,
                Candidates.COLUMN_MUSIC_TASTE,
                Candidates.COLUMN_POLITICAL_PARTY,
                Candidates.COLUMN_RUNNING_OFFICE,
                Candidates.COLUMN_ZIPCODE,
                Candidates.COLUMN_YAY_FACTOR,
                Candidates.COLUMN_ID };
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }
}
