package com.rockthevote.candidate.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Candidates {

    // Database table
    public static final String TABLE_CANDIDATES = "candidates";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PICTURE = "picture";
    public static final String COLUMN_YAYS = "yays";
    public static final String COLUMN_NAYS = "nays";
    public static final String COLUMN_MUSIC_TASTE = "music_taste";
    public static final String COLUMN_POLITICAL_PARTY = "political_party";
    public static final String COLUMN_RUNNING_OFFICE = "running_office";
    public static final String COLUMN_ZIPCODE = "zipcode";
    public static final String COLUMN_YAY_FACTOR = "yay_factor";


    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_CANDIDATES
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_PICTURE + " text not null, "
            + COLUMN_YAYS + " text  not null,"
            + COLUMN_NAYS + " text not null,"
            + COLUMN_MUSIC_TASTE + " text  not null,"
            + COLUMN_POLITICAL_PARTY + " text  not null,"
            + COLUMN_RUNNING_OFFICE + " text  not null,"
            + COLUMN_ZIPCODE + " text  not null,"
            + COLUMN_YAY_FACTOR + " text  not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(Candidates.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_CANDIDATES);
        onCreate(database);
    }

}
