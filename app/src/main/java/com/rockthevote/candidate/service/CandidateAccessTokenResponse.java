package com.rockthevote.candidate.service;

import android.text.TextUtils;

public class CandidateAccessTokenResponse extends CandidateResponse {

    public String access_token;
    public String expires_in;
    public String token_type;
    public String scope;

    @Override
    public Boolean isSuccess() {
        return !TextUtils.isEmpty(access_token);
    }
}
