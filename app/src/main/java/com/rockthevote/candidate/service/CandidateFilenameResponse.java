package com.rockthevote.candidate.service;

public class CandidateFilenameResponse extends CandidateResponse {
    String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
