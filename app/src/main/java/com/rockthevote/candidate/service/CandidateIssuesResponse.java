package com.rockthevote.candidate.service;

import com.rockthevote.candidate.type.Issue;

import java.util.ArrayList;

public class CandidateIssuesResponse extends CandidateResponse {

    public int total;
    public int offset;

    public ArrayList<Issue> rows;

    public void setIssues(ArrayList<Issue> users) {
        this.rows = users;
    }

    public ArrayList<Issue> getIssues() {
        return rows;
    }
}
