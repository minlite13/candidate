package com.rockthevote.candidate.service;

public class CandidateResponse {
    public int code;
    public String message;

    public Boolean isSuccess() {
        return code == 0;
    }
}
