package com.rockthevote.candidate.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.rockthevote.candidate.type.UserIssue;

import java.util.HashMap;
import java.util.Map;

public class CandidateService {
    private static CandidateService ourInstance = new CandidateService();

    public static CandidateService getInstance() {
        return ourInstance;
    }

    private CandidateService() {
    }

    private static final String url = "http://candiate.animaxent.com";

    /** USERS */

    public void getUsers(Context context, Response.Listener<CandidateUsersResponse> listener, Response.ErrorListener errorListener, int page, int total) {
        String parsedUrl = url + "/get_user/list/" + page + "/" + total;
        GsonRequest<CandidateUsersResponse> request = new GsonRequest<>(parsedUrl, CandidateUsersResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void getUserById(Context context, Response.Listener<CandidateUsersResponse> listener, Response.ErrorListener errorListener, String userId) {
        String parsedUrl = url + "/get_user/" + userId;
        GsonRequest<CandidateUsersResponse> request = new GsonRequest<>(parsedUrl, CandidateUsersResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void getUserByFacebookId(Context context, Response.Listener<CandidateUsersResponse> listener, Response.ErrorListener errorListener, String facebookId, String access_token) {
        String parsedUrl = url + "/get_user/fb/" + facebookId + "?access_token=" + access_token;
        GsonRequest<CandidateUsersResponse> request = new GsonRequest<>(parsedUrl, CandidateUsersResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void deleteUserById(Context context, Response.Listener<CandidateQueryResponse> listener, Response.ErrorListener errorListener, String userId) {
        String parsedUrl = url + "/delete_user/" + userId;
        GsonRequest<CandidateQueryResponse> request = new GsonRequest<>(parsedUrl, CandidateQueryResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void loginUser(Context context, Response.Listener<CandidateAccessTokenResponse> listener, Response.ErrorListener errorListener, String userId, String accessToken) {
        String parsedUrl = url + "/login_user/" + userId;

        Map<String, String> params = new HashMap<>();

        params.put("access_token", accessToken);

        GsonPostRequest<CandidateAccessTokenResponse    > request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateAccessTokenResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void logoutUser(Context context, Response.Listener<CandidateResponse> listener, Response.ErrorListener errorListener, String userId) {
        String parsedUrl = url + "/logout_user/" + userId;

        Map<String, String> params = new HashMap<>();

        GsonPostRequest<CandidateResponse> request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }


    public void signUpUser(Context context, Response.Listener<CandidateAccessTokenResponse> listener, Response.ErrorListener errorListener, String firstName, String lastName, String dob, String email, String gender, String facebookId, String picture, String accessToken) {
        String parsedUrl = url + "/signup_user/";

        Map<String, String> params = new HashMap<>();

        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("dob", dob);
        params.put("email", email);
        params.put("gender", gender);
        params.put("facebook_id", facebookId);
        params.put("picture", picture);
        params.put("access_token", accessToken);

        GsonPostRequest<CandidateAccessTokenResponse> request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateAccessTokenResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void updateUserById(Context context, Response.Listener<CandidateQueryResponse> listener, Response.ErrorListener errorListener, String userId, String firstName, String lastName, String age, String bio, String email, String gender, String accessToken) {
        String parsedUrl = url + "/update_user/" + userId + "?access_token=" + accessToken;

        Map<String, String> params = new HashMap<>();

        if(firstName != null) params.put("first_name", firstName);
        if(lastName != null) params.put("last_name", lastName);
        if(age != null) params.put("age", age);
        if(bio != null) params.put("bio", bio);
        if(email != null) params.put("email", email);
        if(gender != null) params.put("gender", gender);

        GsonPostRequest<CandidateQueryResponse> request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateQueryResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /** ISSUES */

    public void getIssues(Context context, Response.Listener<CandidateIssuesResponse> listener, Response.ErrorListener errorListener, int page, int total) {
        String parsedUrl = url + "/get_issue/list/" + page + "/" + total;
        GsonRequest<CandidateIssuesResponse> request = new GsonRequest<>(parsedUrl, CandidateIssuesResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }


    public void getIssueById(Context context, Response.Listener<CandidateIssuesResponse> listener, Response.ErrorListener errorListener, String issueId) {
        String parsedUrl = url + "/get_issue/" + issueId;
        GsonRequest<CandidateIssuesResponse> request = new GsonRequest<>(parsedUrl, CandidateIssuesResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void getIssuesResponseByUserId(Context context, Response.Listener<CandidateUserIssuesResponse> listener, Response.ErrorListener errorListener, String userId) {
        String parsedUrl = url + "/user_issues/" + userId;
        GsonRequest<CandidateUserIssuesResponse> request = new GsonRequest<>(parsedUrl, CandidateUserIssuesResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void setIssuesResponseByUserId(Context context, Response.Listener<CandidateResponse> listener, Response.ErrorListener errorListener, String userId, UserIssue[] userIssues) {
        String parsedUrl = url + "/user_issues/" + userId;

        Map<String, String> params = new HashMap<>();

        String issueParam = "";

        for(UserIssue userIssue : userIssues) {
            issueParam += userIssue.issue_id + ":" + userIssue.response.getValue() + ",";
        }

        params.put("param", issueParam);

        GsonPostRequest<CandidateResponse> request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void getIssueResponseByUserId(Context context, Response.Listener<CandidateUserIssuesResponse> listener, Response.ErrorListener errorListener, String userId, String issueId) {
        String parsedUrl = url + "/user_issues/" + userId + "/" + issueId;
        GsonRequest<CandidateUserIssuesResponse> request = new GsonRequest<>(parsedUrl, CandidateUserIssuesResponse.class, null, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    public void postPicture(Context context, Response.Listener<CandidateFilenameResponse> listener, Response.ErrorListener errorListener, String userId, String slotId, String picData, String extension, String accessToken) {
        String parsedUrl = url + "/post_picture/" + userId + "/" + slotId;

        Map<String, String> params = new HashMap<>();

        params.put("access_token", accessToken);
        params.put("pic_data", picData);
        params.put("extension", extension);

        GsonPostRequest<CandidateFilenameResponse> request = new GsonPostRequest<>(Request.Method.POST, parsedUrl, CandidateFilenameResponse.class, params, listener, errorListener);
        CandidateRequestQueue.getInstance(context).addToRequestQueue(request);
    }
}
