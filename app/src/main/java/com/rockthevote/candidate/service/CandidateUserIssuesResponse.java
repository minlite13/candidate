package com.rockthevote.candidate.service;

import com.rockthevote.candidate.type.UserIssue;

import java.util.ArrayList;

public class CandidateUserIssuesResponse extends CandidateResponse {
    ArrayList<UserIssue> rows;

    public ArrayList<UserIssue> getUserIssues() {
        return rows;
    }

    public void setUserIssues(ArrayList<UserIssue> rows) {
        this.rows = rows;
    }
}
