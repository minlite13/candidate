package com.rockthevote.candidate.service;

import com.rockthevote.candidate.type.User;

import java.util.ArrayList;

public class CandidateUsersResponse extends CandidateResponse {

    public int total;
    public int offset;

    public ArrayList<User> rows;

    public void setUsers(ArrayList<User> users) {
        this.rows = users;
    }

    public ArrayList<User> getUsers() {
        return rows;
    }
}
