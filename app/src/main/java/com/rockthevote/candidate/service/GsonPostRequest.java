package com.rockthevote.candidate.service;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class GsonPostRequest<T> extends Request<T> {
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private Listener<T> listener;
    private Map<String, String> params;

    public GsonPostRequest(String url, Class<T> clazz, Map<String, String> params,
                         Listener<T> responseListener, ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.clazz = clazz;
        this.listener = responseListener;
        this.params = params;
    }

    public GsonPostRequest(int method, String url, Class<T> clazz, Map<String, String> params,
                         Listener<T> responseListener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.listener = responseListener;
        this.params = params;
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        Map<String,String> params = new HashMap<String, String>();
//        params.put("Content-Type","application/x-www-form-urlencoded");
//        return params;
//    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
