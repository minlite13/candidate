package com.rockthevote.candidate.type;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Candidate implements Parcelable {
    public String name;
    public String picture;
    public List<String> yays;
    public List<String> nays;
    public List<String> musicTaste;
    public PoliticalParty politicalParty;
    public String runningOffice;
    public String zipcode;
    public String yay_factor;

    public Candidate(Parcel parcel) {
        this.name = parcel.readString();
        this.picture = parcel.readString();
        yays = parcel.createStringArrayList();
        nays = parcel.createStringArrayList();
        musicTaste = parcel.createStringArrayList();
        politicalParty = PoliticalParty.parseInt(parcel.readInt());
        runningOffice = parcel.readString();
        zipcode = parcel.readString();
        yay_factor = parcel.readString();
    }

    public Candidate(String name, String picture, List<String> yays, List<String> nays, List<String> musicTaste, PoliticalParty politicalParty, String runningOffice,
                     String zipcode, String yay_factor) {
        this.name = name;
        this.picture = picture;
        this.yays = yays;
        this.nays = nays;
        this.musicTaste = musicTaste;
        this.politicalParty = politicalParty;
        this.runningOffice = runningOffice;
        this.zipcode = zipcode;
        this.yay_factor = yay_factor;
    }

    public static enum PoliticalParty {
        REPUBLICAN(0),
        DEMOCRAT(1);

        private int value;

        PoliticalParty(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static PoliticalParty parseInt(int value) {
            switch (value) {
                case 0:
                    return REPUBLICAN;
                case 1:
                default:
                    return DEMOCRAT;
            }
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(picture);
        parcel.writeStringList(yays);
        parcel.writeStringList(nays);
        parcel.writeStringList(musicTaste);
        parcel.writeInt(politicalParty.getValue());
        parcel.writeString(runningOffice);
        parcel.writeString(zipcode);
        parcel.writeString(yay_factor);
    }

    public static Creator<Candidate> CREATOR = new Creator<Candidate>() {
        @Override
        public Candidate createFromParcel(Parcel parcel) {
            return new Candidate(parcel);
        }

        @Override
        public Candidate[] newArray(int size) {
            return new Candidate[size];
        }
    };
}
