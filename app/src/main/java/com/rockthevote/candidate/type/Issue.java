package com.rockthevote.candidate.type;

import android.os.Parcel;
import android.os.Parcelable;

public class Issue implements Parcelable {
    public String id;
    public String issue;

    public Issue(String id, String issue) {
        this.id = id;
        this.issue = issue;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.issue);
    }

    private Issue(Parcel in) {
        this.id = in.readString();
        this.issue = in.readString();
    }

    public static final Parcelable.Creator<Issue> CREATOR = new Parcelable.Creator<Issue>() {
        public Issue createFromParcel(Parcel source) {
            return new Issue(source);
        }

        public Issue[] newArray(int size) {
            return new Issue[size];
        }
    };
}
