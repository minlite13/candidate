package com.rockthevote.candidate.type;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public String id;
    public String first_name;
    public String last_name;
    public String email;
    public String facebook_id;
    public String age;
    public String bio;
    public String gender;
    public String picture;
    public UserPicture pictures;
    public String pattern;

    public User(String id, String first_name, String last_name, String email, String facebook_id, String age, String bio, String gender, String picture, UserPicture pictures, String pattern) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.facebook_id = facebook_id;
        this.age = age;
        this.bio = bio;
        this.gender = gender;
        this.picture = picture;
        this.pictures = pictures;
        this.pattern = pattern;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.email);
        dest.writeString(this.facebook_id);
        dest.writeString(this.age);
        dest.writeString(this.bio);
        dest.writeString(this.gender);
        dest.writeString(this.picture);
        dest.writeParcelable(pictures, 0);
        dest.writeString(this.pattern);
    }

    private User(Parcel in) {
        this.id = in.readString();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.facebook_id = in.readString();
        this.age = in.readString();
        this.bio = in.readString();
        this.gender = in.readString();
        this.picture = in.readString();
        this.pictures = in.readParcelable(UserPicture.class.getClassLoader());
        this.pattern = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
