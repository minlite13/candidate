package com.rockthevote.candidate.type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserIssue implements Parcelable {

    public String id;
    public String user_id;
    public String issue_id;
    public String issue;
    public IssueResponse response;

    public UserIssue(String id, String user_id, String issue_id, String issue, IssueResponse response) {
        this.id = id;
        this.user_id = user_id;
        this.issue_id = issue_id;
        this.issue = issue;
        this.response = response;
    }

    public enum IssueResponse {
        @SerializedName("0")
        NO(0),
        @SerializedName("1")
        YES(1);

        private final int value;

        public int getValue() {
            return value;
        }

        private IssueResponse(int value) {
            this.value = value;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.user_id);
        dest.writeString(this.issue_id);
        dest.writeString(this.issue);
        dest.writeInt(this.response == null ? -1 : this.response.ordinal());
    }

    private UserIssue(Parcel in) {
        this.id = in.readString();
        this.user_id = in.readString();
        this.issue_id = in.readString();
        this.issue = in.readString();
        int tmpResponse = in.readInt();
        this.response = tmpResponse == -1 ? null : IssueResponse.values()[tmpResponse];
    }

    public static final Creator<UserIssue> CREATOR = new Creator<UserIssue>() {
        public UserIssue createFromParcel(Parcel source) {
            return new UserIssue(source);
        }

        public UserIssue[] newArray(int size) {
            return new UserIssue[size];
        }
    };
}
