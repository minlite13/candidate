package com.rockthevote.candidate.type;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

class UserPicture implements Parcelable {
    @SerializedName("1")
    String picture_1;
    @SerializedName("2")
    String picture_2;
    @SerializedName("3")
    String picture_3;
    @SerializedName("4")
    String picture_4;

    UserPicture(String picture_1, String picture_2, String picture_3, String picture_4) {
        this.picture_1 = picture_1;
        this.picture_2 = picture_2;
        this.picture_3 = picture_3;
        this.picture_4 = picture_4;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.picture_1);
        dest.writeString(this.picture_2);
        dest.writeString(this.picture_3);
        dest.writeString(this.picture_4);
    }

    private UserPicture(Parcel in) {
        this.picture_1 = in.readString();
        this.picture_2 = in.readString();
        this.picture_3 = in.readString();
        this.picture_4 = in.readString();
    }

    public static final Creator<UserPicture> CREATOR = new Creator<UserPicture>() {
        public UserPicture createFromParcel(Parcel source) {
            return new UserPicture(source);
        }

        public UserPicture[] newArray(int size) {
            return new UserPicture[size];
        }
    };
}