package com.rockthevote.candidate.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageUtils {
    public static Bitmap bitmapFromUri(Context context, Uri uri, int width, int height) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = context.getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();


            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW/width, photoH/height);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;
            bmOptions.inPurgeable = true;

            return BitmapFactory.decodeFileDescriptor(imageSource, null, bmOptions);

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }

    public static int calculateInSampleSizeWithWidth(
            BitmapFactory.Options options, int reqWidth) {
        // Raw height and width of image
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (width > reqWidth) {
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static int calculateInSampleSizeWithHeight(
            BitmapFactory.Options options, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        int inSampleSize = 1;

        if (height > reqHeight) {
            final int halfHeight = height / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap scaledBitmapFromUriWithLongEdge(Context context, Uri uri, int longEdge) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = context.getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = true;
            bmOptions.inPurgeable = true;

            BitmapFactory.decodeFileDescriptor(imageSource, null, bmOptions);

            int outHeight;
            int outWidth;

            if (bmOptions.outWidth > bmOptions.outHeight) {
                bmOptions.inSampleSize = calculateInSampleSizeWithWidth(bmOptions, longEdge);

                outWidth = longEdge;
                outHeight = (int) (outWidth / (float) bmOptions.outWidth * bmOptions.outHeight);

            } else {
                bmOptions.inSampleSize = calculateInSampleSizeWithHeight(bmOptions, longEdge);

                outHeight = longEdge;
                outWidth = (int) (outHeight / (float) bmOptions.outHeight * bmOptions.outWidth);
            }

            bmOptions.inJustDecodeBounds = false;

            return Bitmap.createScaledBitmap(BitmapFactory.decodeFileDescriptor(imageSource, null, bmOptions), outWidth, outHeight, false);

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }
}
